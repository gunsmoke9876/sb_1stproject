// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Sb_1stProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SB_1STPROJECT_API ASb_1stProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
