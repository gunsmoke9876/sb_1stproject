// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Sb_1stProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Sb_1stProject, "Sb_1stProject" );
